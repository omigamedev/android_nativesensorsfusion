package it.omam.nativepad;

public class SensorFusion 
{
	native static void Init();
	native static void UpdateMagnetometer(float x, float y, float z);
	native static void UpdateAccelerometer(float x, float y, float z);
	native static void UpdateGyroscope(float x, float y, float z);
	native static float GetCurrentAngle();
	native static float[] GetWorldMagnetometer();
	
	static 
	{
	    System.loadLibrary("NativePad");
	}
}
