package it.omam.nativepad;

import it.omam.nativepad.R;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.MotionEvent;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends Activity implements SensorEventListener
{
	public TextView txtBox;
	public ImageView image;
	public Bitmap bitmap;
	public Canvas canvas;
	public Paint paint;
	public int width;
	public int height;

	public SensorManager smgr;
	public Sensor sacc;
	public Sensor smag;
	public Sensor sgyr;
	public int gyroSamples;

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		txtBox = (TextView)findViewById(R.id.logText);
		image = (ImageView)findViewById(R.id.imageView1);
		
		width = 400;
		height = 400;
		bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
		canvas = new Canvas(bitmap);
		paint = new Paint();
		paint.setAntiAlias(false);
		paint.setColor(Color.BLACK);
		paint.setStyle(Paint.Style.STROKE);
		paint.setStrokeWidth(20);

		smgr = (SensorManager) getSystemService(SENSOR_SERVICE);
		sacc = smgr.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		smag = smgr.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
		sgyr = smgr.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
		
		txtBox.setText("Calibrating...");
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) 
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	protected void onResume()
	{
		SensorFusion.Init();
		smgr.registerListener(this, sacc, SensorManager.SENSOR_DELAY_FASTEST);
		smgr.registerListener(this, smag, SensorManager.SENSOR_DELAY_FASTEST);
		smgr.registerListener(this, sgyr, SensorManager.SENSOR_DELAY_FASTEST);
		
		super.onResume();
	}

	@Override
	protected void onPause()
	{
		smgr.unregisterListener(this);
		super.onPause();
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent event) 
	{
		switch(event.getAction())
		{
		case MotionEvent.ACTION_DOWN:
			SensorFusion.Init();
			bitmap.eraseColor(Color.GRAY);
			break;
		case MotionEvent.ACTION_MOVE:
			break;
		case MotionEvent.ACTION_UP:
			break;
		default:
			break;
		}
		return super.onTouchEvent(event);
	}
	
	@Override
	public void onBackPressed() { }

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) { }

	@Override
	public void onSensorChanged(SensorEvent event) 
	{ 
		float x = event.values[0];
		// Swap y and z
		float y = event.values[2];
		float z = event.values[1];
		
		switch(event.sensor.getType())
		{
		case Sensor.TYPE_ACCELEROMETER:
			SensorFusion.UpdateAccelerometer(x, y, z);
			//txtBox.setText(String.format("acc %2.3f %2.3f %2.3f", x, y, z));
			break;
		case Sensor.TYPE_MAGNETIC_FIELD:
			SensorFusion.UpdateMagnetometer(x, y, z);
			if (gyroSamples < 100)
				break;
			
			float[] wmags = SensorFusion.GetWorldMagnetometer();
//			x = wmags[0];
//			y = wmags[1];
//			z = wmags[2];
            //txtBox.setText(String.format("mag\nx = %f\ny = %f\nz = %f\nlen = %f", x, y, z, Math.sqrt(x*x+y*y+z*z)));
            txtBox.setText(String.format("sensor = [%06.2f %06.2f %06.2f]\nworld  = [%06.2f %06.2f %06.2f]\n", 
                    x, y, z, wmags[0], wmags[1], wmags[2]));
//			txtBox.setText("yaw = " + Math.atan2(z, x)/Math.PI*180);
			
			float scale = 6.f;
			float px = (width/2) + -z * scale;
			float py = (height/2) + -x * scale;
			bitmap.eraseColor(Color.GRAY);
			canvas.drawPoint(width/2, height/2, paint);
			canvas.drawPoint(px, py, paint);
			image.setImageBitmap(bitmap);
			break;
		case Sensor.TYPE_GYROSCOPE:
			SensorFusion.UpdateGyroscope(x, y, z);
			gyroSamples++;
			break;
		}
		
//		txtBox.setText("yaw = " + SensorFusion.GetCurrentAngle());
	}
	
}
