// Overcome an Eclipse bug that does not recognise C++11
#if __cplusplus < 201103L
	#define __cplusplus 201103L
#endif

#define LOG(fmt,...) __android_log_print(ANDROID_LOG_INFO, "native", fmt, ##__VA_ARGS__)
#define ERR(fmt,...) __android_log_print(ANDROID_LOG_ERROR, "native",\
    "%s:%d " fmt, __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__)

#include <chrono>
#include <vector>

#define GLM_MESSAGES
#define GLM_SWIZZLE
#define GLM_FORCE_RADIANS
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/type_ptr.hpp"
#include "glm/gtc/quaternion.hpp"
#include "glm/gtx/vector_angle.hpp"
#include "glm/gtx/euler_angles.hpp"

#include <jni.h>
#include <android/native_activity.h>
#include <android/log.h>
#include <android/looper.h>
#include <android/configuration.h>
#include <android/sensor.h>

#include "it_omam_nativepad_SensorFusion.h"
#include "cbuffer.h"
